conx = 6.5;
cony = 20.5;
conh = 15;
consp = 41.5;
t = 3;

r1 = 1.25;
hsep = 10.18;

holesepH = 46.10+1.25;
holesepV = 9+1.25;
l = consp*2+conx*3;
w = cony+t;




$fn=90;

cube([consp, cony, conh], center=true);
for(i = [
        [holesepH/2, holesepV/2,0],
        [-holesepH/2, holesepV/2,0],
        [holesepH/2, -holesepV/2,0],
        [-holesepH/2, -holesepV/2,0]
    ]) {
    translate(i) {
        cylinder(r=r1, h=conh, center=true);
    }
}