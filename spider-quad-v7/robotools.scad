module boxChafer(w,h,t, center = false) {
    difference() {
        cylinder(r=w/2, h=h, center=center);
        translate([0,0,-.1])
        cylinder(r=w/2-t/2, h=h+1, center=center);
    }
}