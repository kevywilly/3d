

w = 20;     //inner width
l = 40.65;  //inner length
t = 1;      //wall thickness
b = 2;      //base thickness
h1 = 8;     //height lower end
h2 = 10;    //Height higher end
w2 = 8;     //Width of cutout for wires
r1 = 3.5;     //Radius of pin hole
r2 = 6.5;   //Radius of pin hole cutout

xhole = (9.8 + t);



$fn=90;


difference() {
translate([(l+2*t)/2,0,h2/2]) {
    
    ex = 2;
    ey = 1.5;
 

    difference() {
        // outer box
        cube([l+2*t,w+2*t,h2], center=true);
        
        // inner box
        translate([0,0,b]) {
            cube([l,w,h2], center=true); 
        }
        
        // wire slot
        translate([-l/2,0,b]) {
            cube([10,8,h2],center=true);
        }
        
      

    }
}


// lower side

translate([l*.9,-(w+2*t+.2)/2,h1]) {
    cube([l,w+2*t+.2,h2]);
}



// inner hole    
translate([xhole,0,-.1]) {
cylinder(r=r1, h=b+.2);
}

// hole offset
translate([xhole,0,b/2]) {
cylinder(r=r2, h=b+.2);
}
}