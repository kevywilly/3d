

r1 = 98/2;
hSmall = 1.25;
hBig = 1.5;
t = 3.0;
hSide = 4.25;


// Main Base


// Board 1 holes (+/- 24.75,0), (+/- 24.75,20)


difference() {
    union() {
    bottom();
    centerBox();
    //translate([1,11,-1.5])
    //import("legacy-boards-base.stl", convexity=3);
    }
    // Middle Hole
    $fn=90;
    cylinder(r=hSmall, h=t+1, center=true);
    topHoles();
    board1Holes();
    board2Holes();
    bottomHoles();
    sideWireHoles();
    topBottomWireHoles();
}


module bottom() {
   $fn=20;
   color("red")
   cylinder(r=r1, h=3, center=true); 
}

// Utility for chafering edges of a box.
module boxChafer(w,h,t, center = false) {
    difference() {
        cylinder(r=w/2, h=h, center=center);
        translate([0,0,-.1])
        cylinder(r=w/2-t/2, h=h+1, center=center);
    }
}
module centerBox() {
    s = 14;
    h = 11.5;
    
    color("red")
    rotate([0,0,45]) {
        difference() {
            translate([0,0,h/2]) {   
                cube([s,s,h], center=true);
            }
            
            translate([0,0,h/2+hSmall/2]) {
            rotate([0,90,0])
            cylinder(r=hSmall, h=s*2, center=true);
            }
            
            translate([0,0,h/2+hSmall/2]) {
            rotate([90,0,0])
            cylinder(r=hSmall, h=s*2, center=true);
            }
            boxChafer(sqrt(15*15+15*15),12,3);
            
        }
    }
}

module topBottomWireHoles() {
    $fn=90;
    
    color("red") {
      
        scaleFactor = [1,3.25,1];
        radius = hSide*.75;
        
        translate([30,30,0])
        rotate([0,0,45])
        scale(scaleFactor)
        cylinder(r=radius, h=t*3, center=true);
        
        translate([-30,30,0])
        rotate([0,0,-45])
        scale(scaleFactor)
        cylinder(r=radius, h=t*3, center=true);
        
        translate([-20,-35,0])
        rotate([0,0,60])
        scale(scaleFactor)
        cylinder(r=radius, h=t*3, center=true);
          
        translate([20,-35,0])
        rotate([0,0,-60])
        scale(scaleFactor)
        cylinder(r=radius, h=t*3, center=true);
          
    }
  
    
}

module sideWireHoles() {
       $fn=90;
    color("red")
    for(i=[r1-7.5,-r1+7.5]) {  
        scale([1,4.25,1])
        translate([i,0,0]) {
            cylinder(r=hSide, h=t*3, center=true);
        }   
    }   
    
}
module topHoles() {
  $fn=90;
      color("red")
    for(i=[15.5,-15.5]) {
        for(j=[98/2 - 14]) {
        translate([i,j,0]) {
            cylinder(r=hSmall, h=t*3, center=true);
        }
        }
    }  
}

//upper middle holes
module board1Holes() {

  $fn=90;
      color("red")
    for(i=[28,-28]) {
        for(j=[1.25,21.25]) {
        translate([i,j,0]) {
            cylinder(r=hSmall, h=t*3, center=true);
        }
        }
    }  
}

//bottom middle holes
module board2Holes() {
  $fn=90;
      color("red")
    for(i=[33,-33]) {
        for(j=[-3.75,-29.75]) {
        translate([i,j,0]) {
            cylinder(r=hSmall, h=t*3, center=true);
        }
        }
    }  
}


module bottomHoles() {
  $fn=90;
      color("red")
    for(i=[10,-10]) {
        for(j=[-98/2+6.5]) {
        translate([i,j,0]) {
            cylinder(r=hSmall, h=t*3, center=true);
        }
        }
    }  
}