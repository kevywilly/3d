

w = 20;
l = 40.65;
t = 1;
b = 3;
h1 = 8;
h2 = 12;
w2 = 8;
r1 = 3;
r2 = 5.25;

xhole = (r2 + 4.55 + t);

$fn=90;


difference() {
translate([(l+2*t)/2,0,h2/2]) {
    difference() {
        // outer box
        cube([l+2*t,w+2*t,h2], center=true);
        
        // inner box
        translate([0,0,b]) {
            cube([l,w,h2], center=true); 
        }
        
        // wire slot
        translate([-w,0,3]) {
            cube([10,8,h2],center=true);
        }
        
      

    }
}

// lower side
translate([l*.8,-(w+2*t+.2)/2,8]) {
    cube([l,w+2*t+.2,h2]);
}

// lower side
translate([l*.75,-w/2,3]) {
    cube([l,w,h2]);
}

// inner hole    
translate([xhole,0,-.1]) {
cylinder(r=r1, h=b+.2);
}

// hole offset
translate([xhole,0,b/2]) {
cylinder(r=r2, h=b+.2);
}
}