

// Shoulder settings
r1 = 16;
r2 = 13;
r3 = 5.25;
r4 = 3.5;
t = 4.5;
smallscrew = 1.0;
medscrew = 1.25;
shouldervspace = 24.2;
shoulderhspace = 42.5;
wbase = 25;
hbase = 6;

length = shoulderhspace+2*t;
$fn=90;

 shoulder(holes = true);
//hip();


module hip() {
 //translate([0,0,length])
 //rotate([0,90,0]) {
     shoulder(holes=false);
    
    translate([r1 + length/2,length/2 + r1,hbase])
    rotate([0,180,90])
    shoulder(holes=false);  
 //} 
    
}

module shoulder(holes = false) {
    shoulderbase(holes);

    translate([0,0,t]) {
    shoulderarm1();
    translate([t+shoulderhspace,0,0])
    shoulderarm2();
    }

}

module shoulderbase(holes = false) {
    translate([(shoulderhspace+2*t)/2,(wbase+(2*r1-wbase))/2,hbase/2])
    difference() {
        
    // full base
    cube([shoulderhspace+2*t,wbase,hbase], center=true);
        
    if(holes) {
    // cutout add 2.5mm for clearance
    translate([0,0,hbase/2])
    cube([wbase+2.5,r1*2+.2,hbase+.25],center=true);
        
        for(i=[-5,5]) {
            translate([0,i,0]) {
                cylinder(r=medscrew, h=hbase+.2, center=true);
            }
            translate([i,0,0]) {
                cylinder(r=medscrew, h=hbase+.2, center=true);
            }
        }
    } 
    } 
}

module shoulderarm1() { 
    translate([t/2,r1,shouldervspace])
    rotate([0,90,180])
    difference() {
        union() {
            cylinder(r=r1, h=t, center=true);
            translate([0,-r1,-t/2]) {
            cube([shouldervspace+t,r1*2,t]);
            }
        }
        translate([0,0,t/2+.1])
        cylinder(r=r2, h=t, center=true);
        cylinder(r=r3, h=t+.2, center=true); 
        for(i=[-17/2,17/2]) {
            translate([i,0,0]) {
                cylinder(r=smallscrew, h=t*2, center=true);
            }
        }
    }

}

module shoulderarm2() {

    translate([t/2,r1,shouldervspace])
    rotate([0,90,180])
    difference() {
        union() {
            cylinder(r=r1, h=t, center=true);
            translate([0,-r1,-t/2]) {
            cube([shouldervspace+t,r1*2,t]);
            }
        }
        
        cylinder(r=r4, h=t+.2, center=true); 
        
    }

}

module servo_box_pin() {
    $fn=90;
    cylinder(r=9.75/2, h=1.25);
    cylinder(r=5.25/2, h=10);
}

