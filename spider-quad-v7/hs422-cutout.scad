


$fn=50;

//hs422cutout();

// 

module cutoutOneSide(t,h) {
    difference() {
        roundedCube(x=47, y=24.5, z=4, r = 5, center=true);
        hs422cutout();
    }
    
}
module hs422cutout(sx=41, sy=20.5, hx=47.35, hy=10.18, z=15, r=1.25) {
cube([sx, sy, z], center=true);
for(i = [
        [hx/2, hy/2,0],
        [-hx/2, hy/2,0],
        [hx/2, -hy/2,0],
        [-hx/2, -hy/2,0]
    ]) {
    translate(i) {
        cylinder(r=r, h=z, center=true);
    }
}
}

module roundedCube(x,y,z,r,center=false) {
    minkowski() {
        cube([x-2*r,y-2*r,z],center=center);
        cylinder(r=r,h=z, center=center);
    }
}