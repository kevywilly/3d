include <robotools.scad>;

conx = 6.5;
cony = 20.5;
conh = 15;
consp = 41.5;
t = 3;

r1 = 1.25;
hsep = 10.18;

holesepH = 46.10+1.25;
holesepV = 9+1.25;
l = consp*2+conx*3;
w = cony+t;

rotate([-90,0,0])
femur();



$fn=90;

module femur() {
    
    difference() {
        union() {
            translate([l/2,cony/2,conh/2]) {
                difference() {

                    // box for connectors
                    cube([l,cony,conh], center=true);

                    // space1 between connectors
                    translate([-consp/2-conx/2,0,0])
                    cube([consp,cony+.2,cony+.2],center=true);
                        
                    translate([consp/2+conx/2,0,0])
                    cube([consp,cony+.2,cony+.2],center=true);
                        
                    for(i=[-holesepH,0,holesepH]) {
                        for(j=[-holesepV/2,holesepV/2]) {
                            translate([i,j,0])
                            cylinder(r=r1,h=cony+.2, center=true);
                        }
                    }
                }
            }

             // layout the connector bqr
            translate([0,cony,0])
            cube([l,t,conh]);
        }
            
         // edges 
         for(i=[[-7,-3],[-7,cony+t+3],[l-7,-3],[l-7,cony+t+3]]) {
            translate([i[0],i[1],-.1])
            rotate([0,0,-45])
            cube([10,10,conh+.2]);
            }  
          
          translate([-.1,-.1,conh/2+.1])
          cube([conx+.2,cony+t+.22,conh/2]);  
            
          translate([l-conx-.1,-.1,-.1])
          cube([conx+.2,cony+t+.2,conh/2]);  
    } 


}