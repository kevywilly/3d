
$fn=90;

//bigger pin
//pin(3.2,10,5,1.25);

//smaller pin

pin(2.5,10,5,1.25);

module pin(r1=3, h1=10, r2=5,  h2 = 1.25) {
cylinder(r=r2, h=h2);
cylinder(r=r1, h=h1);
    
}