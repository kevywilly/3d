rHornFrame = 16;
rHorn = 12.5; 
rHornHole = 9.5/2;
hornz = 2.1;
hornHoleSpacing = 14.0;

r4 = 3.5;
t = 3;
medScrew = 1.5;
smallScrew = 2.25/2;

x = 150;
y = 150;

radiusx = x/2;
radiusy = y/2;


hx = sin(40)*radiusx;
//hx = sqrt(radiusx*radiusx/2);
hy = cos(40)*radiusx; //(radiusy*radiusy/2);

$fn=90;

horns = [[hx,hy,0],[-hx,hy,0],[hx,-hy,0],[-hx,-hy,0]];

holes = true;

difference() {
    
        translate([0,0,t/2]) {
            difference() {
            body();
            hornHoles(holes);
            }
        }
}

module hornHoles(horn = true) {
    for(i=horns) {
        
        translate(i) {
            if(horn == true) {
                translate([0,0,hornz])
                // partial cutout
                cylinder(r=rHorn, h=t, $fn=90);
                
                // shaft hole
                translate([0,0,-.1])
                cylinder(r=rHornHole, h=t*2, $fn=90);
                
                for(j=[7,-7]) {
                    translate([j,0,-.1])
                    cylinder(r=smallScrew, h=t*2, $fn=90);
                    translate([0,j,-.1])
                    cylinder(r=smallScrew, h=t*2, $fn=90);
                  }
                  
                for(j=[-22.5,22.5]) {
                    for(k=[-37.5,37.5]) {
                        translate([j,k,0])
                        cube([5,30,t*3],center=true);
                    }
                }
            } else {
               translate([0,0,-.1])
               cylinder(r=rHornHole/2, h=t*2, $fn=90);  
            }
        }
        
        // shaft hole
       
         
    }
}
module body() {
    hornFrames();
    difference() {
        fx = 1.425;
        bodyCircle();
        color("red")
       
        scale([1.25,1,1.2]) {
            translate([radiusx*fx,0,-.1])
            bodyCircle();  
            translate([-radiusx*fx,0,-.10])
            bodyCircle(); 
        } 
        
        fy = 1.675;
        scale([.75,1,1.2]) {
            translate([0,radiusy*fy,-.1])
            bodyCircle();  
            translate([0,-radiusy*fy,-.1])
            bodyCircle(); 
        } 
              
          
    }
}

module bodyCircle() {
    scale([1,y/x,1]) {
        cylinder(r=radiusx, h=t, $fn=90);
    }
}

module hornFrames() {
    for(i=horns) {
        translate(i)
            cylinder(r=rHornFrame,h=t+1, $fn=90);
    }
}

//baseBody(); 






/*
for(i=[[x,x,0],[-x,x,0],[x,-x,0],[-x,-x,0],[hypotenuse-rHorn,0,0],[-hypotenuse,0,0]]) {
    translate(i)
    
}
*/